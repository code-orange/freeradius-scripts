#!/usr/bin/python3

import pymysql

from decouple import config

DATABASE_HOST = config("DATABASE_HOST", default="localhost", cast=str)
DATABASE_USERNAME = config("DATABASE_USERNAME", default="root", cast=str)
DATABASE_PASSWD = config("DATABASE_PASSWD", default="", cast=str)
DATABASE_DBNAME = config("DATABASE_DBNAME", default="radius", cast=str)

file_intro = "#ZONE   HOST(S)                 OPTIONS"

# Open database connection
db = pymysql.connect(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWD, DATABASE_DBNAME)

# prepare a cursor object using cursor() method
cursor = db.cursor()

# execute SQL query using execute() method.
cursor.execute("SELECT nasname, shortname FROM nas ORDER BY shortname")

# Fetch a single row using fetchone() method.
data = cursor.fetchall()

shorewall_hosts = open("/etc/shorewall/hosts", "wt")
shorewall_hosts.write(file_intro)

for row in data:
    shorewall_hosts.write("# " + row[1] + "\r\n")
    shorewall_hosts.write("rad         eth0:" + row[0] + "\r\n" + "\r\n")

shorewall_hosts.close()

# disconnect from server
db.close()
